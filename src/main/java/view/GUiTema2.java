package view;


import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUiTema2 {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField text1;
	private JTextField text2;
	private JTextField text3;
	private int minService;
	private int maxService;
	private JTextArea textArea;
	private boolean confirm;
	private int nrClienti;
	private int minArrival;
	private int maxArrival;
	private int simtime;

	public GUiTema2() {
		frame = new JFrame("Aplicatie Queue Management");
		frame.setBounds(100, 100, 700, 613);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(85, 102, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(319, 102, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton confirmButton = new JButton("Confirm setup");
		confirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String aux1=textField.getText();
				String aux2=textField_1.getText();
				String aux3=textField_2.getText();
				String aux4=textField_3.getText();
				String aux5=textField_4.getText();
				String aux6=textField_5.getText();
				minService=Integer.parseInt(aux1);
				maxService=Integer.parseInt(aux2);
				nrClienti=Integer.parseInt(aux3);
				minArrival=Integer.parseInt(aux4);
				maxArrival=Integer.parseInt(aux5);
				simtime=Integer.parseInt(aux6);
				confirm=true;
			}
		});
		confirmButton.setBounds(189, 240, 118, 23);
		frame.getContentPane().add(confirmButton);
		
		JLabel minimumServiceTime = new JLabel("Minimum service time");
		minimumServiceTime.setBounds(79, 77, 135, 14);
		frame.getContentPane().add(minimumServiceTime);
		
		JLabel maximumServiceTime = new JLabel("Maximum service time");
		maximumServiceTime.setBounds(307, 77, 164, 14);
		frame.getContentPane().add(maximumServiceTime);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(38, 312, 439, 220);
		frame.getContentPane().add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		textField_3 = new JTextField();
		textField_3.setBounds(85, 170, 86, 20);
		frame.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(319, 170, 86, 20);
		frame.getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		text1 = new JTextField();
		text1.setBounds(500, 170, 100, 20);
		frame.getContentPane().add(text1);
		text1.setColumns(10);
		
		text2 = new JTextField();
		text2.setBounds(500, 46, 100, 20);
		frame.getContentPane().add(text2);
		text2.setColumns(10);
	
		text3 = new JTextField();
		text3.setBounds(500, 102, 100, 20);
		frame.getContentPane().add(text3);
		text3.setColumns(10);
		
		JLabel minimumArrivalTime = new JLabel("Minimum arrival time");
		minimumArrivalTime.setBounds(74, 145, 164, 14);
		frame.getContentPane().add(minimumArrivalTime);
		
		JLabel maximumArrivalTime = new JLabel("Maximum arrival time");
		maximumArrivalTime.setBounds(307, 145, 164, 14);
		frame.getContentPane().add(maximumArrivalTime);
		
		textField_2 = new JTextField();
		textField_2.setBounds(85, 46, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JLabel numarClienti = new JLabel("Numar clienti");
		numarClienti.setBounds(96, 21, 86, 14);
		frame.getContentPane().add(numarClienti);
		
		textField_5 = new JTextField();
		textField_5.setBounds(319, 46, 86, 20);
		frame.getContentPane().add(textField_5);
		textField_5.setColumns(10);
		
		JLabel simtime = new JLabel("Simulation time");
		simtime.setBounds(307, 21, 164, 14);
		frame.getContentPane().add(simtime);
		
		confirm=false;
	}
	public int getNr() {
		return nrClienti;
	}
	public int getMax() {
		return maxService;
	}
	public int getMin() {
		return minService;
	}
	public void writeLog(String log) {
		textArea.setText(log);
	}
	public JFrame getFrame() {
		return frame;
	}
	public boolean getStatus() {
		return confirm;
	}
	public JTextArea getA() {
		return textArea;
	}
	public void appendLog(String log) {
		textArea.append(log);
	}
	public int getArMin() {
		return minArrival;
	}
	public int getArMax() {
		return maxArrival;
	}
	public int getSimtime() {
		return simtime;
	}
	public JTextField getT1() {
		return text1;
	}
	public JTextField getT2() {
		return text2;
	}
	public JTextField getT3() {
		return text3;
	}
}
