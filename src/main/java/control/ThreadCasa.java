package control;
import java.util.*;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.*;
import model.Client;
import model.Coada;
import view.GUiTema2;

public class ThreadCasa extends Thread{
	private Coada coadaCasa;
	private Client clientCasa = null;
	private int clientTime = 0;
	private int time=0;
	private boolean ok = true;
	private String nume;
	private int totalService;
	private ArrayList<Client> oameni;
	private int wait;
	private int service;
	private int count;
	private int max,peak;
	private JTextArea textArea;
	private JTextField textField;
	private String evol;
	
	public ThreadCasa(String nume) {
		this.nume=nume;
		coadaCasa=new Coada();
		oameni= new ArrayList<Client>();
		wait=0;count=0;service=0;max=0;peak=0;
		evol="";
	}
	
	
	public void run() {
		while(true && ok) {
			if(max < coadaCasa.getCoada().size()) {
				max=coadaCasa.getCoada().size();
				peak=time;
			}
			int i=0;
			while(i<coadaCasa.getCoada().size())
				{evol+="*";
				i++;
				}
			textField.setText(evol);
			time++;
			if(clientTime == 0 && !coadaCasa.getCoada().isEmpty()) {
				clientCasa = coadaCasa.getCoada().remove();
				clientTime = clientCasa.getServiceTime();
				service+=clientCasa.getServiceTime();
				clientCasa.setCoada(nume);
				count++;
			}
			if( clientTime != 0) {
				clientTime-- ;
				totalService--;
				if(clientTime == 0) {
					clientCasa.setFinishTime(time);
					evol="";
					while(i<coadaCasa.getCoada().size())
					{evol+="*";
					i++;
					}
				textField.setText(evol);
					wait=wait+clientCasa.getFinishTime()-clientCasa.getArrivalTime();
					textArea.append("Clientul "+clientCasa.getNume()+" a terminat la coada "+clientCasa.getNrCoada()+" la momentul "+ clientCasa.getFinishTime()+"\n");
					//System.out.println(clientCasa.toString());
					oameni.add(clientCasa); 
				}
			}
			try {
				Thread.sleep(1000);
				}
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println(time +" Total Service TIme: "+totalService);
		}
	}
	
	public void addClientQueue(Client client) {
		client.setArrivalTime(time);
		client.setCoada(nume);
		textArea.append("Clientul "+client.getNume()+" a ajuns la coada "+ client.getNrCoada()+" la momentul "+client.getArrivalTime()+"\n");
		totalService+=client.getServiceTime();
		coadaCasa.add(client);
	}
	public int getTotalTime() {
		return totalService;
	}
	public ArrayList<Client> getOameni() {
		return oameni;
	}
	public double getAverage() {
		if (count==0) return 0;
		return wait/count;
	}
	public double getAvgSer() {
		if (count==0) return 0;
		return service/count;
	}
	public int getPeak() {
		return peak;
	}
	public int getTime() {
		return time;
	}
	public void setText(JTextArea textArea) {
		this.textArea=textArea;	
	}
	public void sendText(JTextField text) {
		textField=text;
	}
	public JTextField getText() {
		return textField;
	}
}