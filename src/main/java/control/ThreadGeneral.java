package control;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import javax.swing.JTextArea;

import model.*;
import model.Client;
import view.GUiTema2;


public class ThreadGeneral extends Thread implements Runnable{
	static ThreadCasa th = new ThreadCasa("Casa 1");
	static ThreadCasa th2=new ThreadCasa("Casa 2");
	static ThreadCasa th3= new ThreadCasa("casa 3");
	Queue<Client> coadaMagazin = new LinkedList<Client>();
	ArrayList<Client> oameni=new ArrayList<Client>();
	private double avg1,avg2,avg3,avgw1,avgw2,avgw3;
	int minArrival,maxArrival;
	String peak="";
	Random randArrival=new Random();
	int simtime;
	private JTextArea textArea;
	private GUiTema2 gui;
	public void run() {
		th.setText(textArea);
		th2.setText(textArea);
		th3.setText(textArea);
		th.sendText(gui.getT1());
		th2.sendText(gui.getT2());
		th3.sendText(gui.getT3());
		th.start();
		th2.start();
		th3.start();
		while(true) {

			try {
				Thread.sleep((randArrival.nextInt(maxArrival-minArrival+1)+minArrival)*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("time :" + time);
			if(!coadaMagazin.isEmpty() && th.getTime()<simtime)
			{if(th.getTotalTime() < th2.getTotalTime()) 
				th.addClientQueue(coadaMagazin.remove());
			else if( th2.getTotalTime() < th3.getTotalTime())
				th2.addClientQueue(coadaMagazin.remove());
			else th3.addClientQueue(coadaMagazin.remove());}
			else 
			{
				//System.out.println("end bucla while");
				if(th.getTotalTime()<=0 && th2.getTotalTime()<=0 && th3.getTotalTime()<=0)
				{th.stop();
				th2.stop();
				th3.stop();
				th.getText().setText("");
				th2.getText().setText("");
				th3.getText().setText("");
				avg1=th.getAverage();
				avgw1=th.getAvgSer();
				avg2=th2.getAverage();
				avg3=th3.getAverage();
				avgw2=th2.getAvgSer();
				avgw3=th3.getAvgSer();
				peak=peak+"Peak time Coada 1: "+th.getPeak()+"\n";
				peak=peak+"Peak time Coada 2: "+th2.getPeak()+"\n";
				peak=peak+"Peak time Coada 3: "+th3.getPeak()+"\n";
				
				oameni.addAll(th.getOameni());
				oameni.addAll(th2.getOameni());
				oameni.addAll(th3.getOameni());
				this.stop();
				}
			}
		//if(th.getStatus()==1 && th2.getStatus()==1 && th3.getStatus()==1)
		//log+=th.getLog()+th2.getLog()+th3.getLog();
		//System.out.print(th.getLog());
		}
	}
	public void setCoadaAsteptare(Queue<Client> coada) {
		coadaMagazin=coada;
	}
	public ArrayList<Client> getOameni(){
		return oameni;
	}
	public double getAVG1() {
		return avg1;
	}
	public double getAVGS1() {
		return avgw1;
	}
	public double getAVG2() {
		return avg2;
	}
	public double getAVGS2() {
		return avgw2;
	}
	public double getAVG3() {
		return avg3;
	}
	public double getAVGS3() {
		return avgw3;
	}
	public void setArrival(int minArrival,int maxArrival) {
		this.minArrival=minArrival;
		this.maxArrival=maxArrival;

	}
	public String getPeak() {
		return peak;
	}
	public void setSim(int simtime) {
		this.simtime=simtime;
	}
	public void setText(JTextArea textArea) {
		this.textArea=textArea;
	}
	public void sendGUI(GUiTema2 gui) {
		this.gui=gui;
	}
}