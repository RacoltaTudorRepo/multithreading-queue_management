package launch;
import view.*;
import java.util.*;

import control.*;
import model.Client;
public class Main {
	public static void main(String[] args) throws InterruptedException {
		GUiTema2 gui=new GUiTema2();
		gui.getFrame().setVisible(true);
		while(gui.getStatus()==false) {	
			System.out.print("");
		}
		int minService=gui.getMin();
		int maxService=gui.getMax();
		int nrClienti=gui.getNr();
		int minArrival=gui.getArMin();
		int maxArrival=gui.getArMax();
		
		Random randService=new Random();	
		int randST;
		
		String arrayNume[]=new String[20];
		arrayNume[0]=new String("Gigel");
		arrayNume[1]=new String("Dorel");
		arrayNume[2]=new String("Adrian");
		arrayNume[3]=new String("Ioan");
		arrayNume[4]=new String("Mircea");
		arrayNume[5]=new String("Alexandru");
		arrayNume[6]=new String("Popescu");
		arrayNume[7]=new String("Puiu");
		arrayNume[8]=new String("Daniel");
		arrayNume[9]=new String("Radu");
		arrayNume[10]=new String("Matei");
		
		Queue<Client> coadaMagazin = new LinkedList<Client>();
		for(int i=0;i<nrClienti;i++)
		{
		randST= randService.nextInt(maxService-minService+1)+minService;
		coadaMagazin.add(new Client(arrayNume[i], randST));
		}
		
		String log="";
		Iterator<Client> it=coadaMagazin.iterator();
		log=log+"Date initiale despre clientii generati:\n";
		while(it.hasNext())
			log=log+it.next().getInitial()+"\n";
		gui.writeLog(log);	
		gui.appendLog("\n");
		ThreadGeneral th = new ThreadGeneral();
		th.setCoadaAsteptare(coadaMagazin);
		th.setArrival(minArrival, maxArrival);
		th.setSim(gui.getSimtime());
		th.setText(gui.getA());
		th.start();
		th.sendGUI(gui);
		th.join();
		gui.appendLog("\n");
		ArrayList<Client> oameni= th.getOameni();
		for(int i=0;i<oameni.size();i++)
			gui.appendLog(oameni.get(i).toString()+"\n");
		gui.appendLog("Average time: "+ th.getAVG1() +" al cozii Coada 1\n");
		gui.appendLog("Average waiting time: "+ th.getAVGS1() +" al cozii Coada 1\n");
		gui.appendLog("Average time: "+ th.getAVG2() +" al cozii Coada 2\n");
		gui.appendLog("Average waiting time: "+ th.getAVGS2() +" al cozii Coada 2\n");
		gui.appendLog("Average time: "+ th.getAVG3() +" al cozii Coada 3\n");
		gui.appendLog("Average waiting time: "+ th.getAVGS3() +" al cozii Coada 3\n");
		gui.appendLog(th.getPeak());
	}
}
