package model;

public class Client {
	private String nume;
	private int arrivalTime;
	private int serviceTime;
	private int finishTime;
	private String numar;

	public Client(String nume) {
		this.nume = nume;
	}

	public Client(String nume, int serviceTime) {
		this.nume = nume;
		this.serviceTime = serviceTime;
	}

	public String getNume() {
		return nume;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public void setSerivceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}

	public int getServiceTime() {
		return serviceTime;
	}

	public int getFinishTime() {
		return finishTime;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setCoada(String numar) {
		this.numar = numar;
	}

	public String getNrCoada() {
		return numar;
	}

	public String toString() {
		return "Clientul " + nume + " arrival time: " + arrivalTime + " finish time: " + finishTime + " in coada "
				+ numar;
	}

	public String getInitial() {
		return "Clientul " + nume + " service time: " + serviceTime;
	}
}
