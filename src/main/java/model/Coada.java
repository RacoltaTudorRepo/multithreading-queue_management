package model;

import java.util.LinkedList;
import java.util.Queue;

public class Coada {
	private Queue<Client> coada;
	private int totalTime;

	// Constructor implicit
	public Coada() {
		coada = new LinkedList<Client>();
	}

	public void add(Client client) {
		coada.add(client);
		totalTime += client.getServiceTime();
	}

	public void remove() {
		coada.remove();
	}

	public int getTotalTime() {
		return totalTime;
	}

	public Queue<Client> getCoada() {
		return coada;
	}
}
